class Potepan::ProductsController < ApplicationController
  MAX_DISPLAY_NUM_OF_PRODUCTS = 4

  def show
    @product = Spree::Product.includes(product_properties: :property).find(params[:id])
    @related_products = Spree::Product.includes(master: [:default_price, :images]).
      get_related_products(@product).limit(MAX_DISPLAY_NUM_OF_PRODUCTS)
  end
end

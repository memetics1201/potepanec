class Potepan::CategoriesController < ApplicationController
  def show
    @option_types = Spree::OptionType.all.includes(:option_values)
    @taxonomies = Spree::Taxonomy.all.includes(root: [children: :children])
    @taxon = Spree::Taxon.includes(
      products: { master: [:default_price, :images] }
    ).find(params[:id])
  end
end

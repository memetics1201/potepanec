Spree::Product.class_eval do

  MAX_NUM_OF_NEW_PRODUCTS = 8

  scope :get_related_products, ->(product) {
    joins(:taxons).
      where(spree_taxons: { id: product.taxons.ids }).
      where.not(id: product.id).
      distinct
  }

  scope :get_eight_new_products, -> {
    includes(master: [:default_price, :images]).all.order(available_on: "ASC").limit(MAX_NUM_OF_NEW_PRODUCTS)
  }
end

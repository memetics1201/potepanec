require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  describe "show a list of products by categories" do
    given!(:taxonomy) { create(:taxonomy, name: "taxonomy") }
    given!(:taxon) { create(:taxon, name: "taxon", taxonomy: taxonomy, parent: taxonomy.root) }
    given!(:child_taxon) { create(:taxon, name: "child taxon", taxonomy: taxonomy, parent: taxon) }
    given!(:product) { create(:product, taxons: [taxon]) }
    given!(:option_type) { create(:option_type) }
    given!(:option_value) { create(:option_value, option_type: option_type) }

    background do
      visit potepan_category_path(taxon.id)
    end

    scenario "user visits a product category page" do
      within("section.pageHeader") do
        expect(page).to have_content taxon.name
        expect(page).to have_content taxon.root.name
      end
      within("div.sideBar") do
        expect(page).to have_css "#taxonomy_#{taxonomy.id}"
        expect(page).to have_content taxonomy.name
        expect(page).to have_css "#taxon_#{taxon.id}"
        expect(page).to have_content taxon.name
        expect(page).to have_content taxon.classifications_count
        expect(page).to have_content child_taxon.name
        expect(page).to have_content child_taxon.classifications_count
        expect(page).to have_content option_type.presentation
        expect(page).to have_content option_value.option_values_variants_count
      end
      within("div.productBox") do
        expect(page).to have_link product.name
        expect(page).to have_link product.display_price
      end
    end

    scenario "user clicks potepan_product_path" do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    scenario "user clicks potepan_category_path" do
      click_link child_taxon.name
      expect(current_path).to eq potepan_category_path(child_taxon.id)
    end
  end
end

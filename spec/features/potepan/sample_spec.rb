require 'rails_helper'

RSpec.feature "Sample", type: :feature do
  describe "show new products" do
    given(:products) { create_list(:product, 8) }

    background do
      visit potepan_index_path
    end

    scenario "user visits a index page" do
      within("div.featuredProductsSlider") do
        expect(page).to have_content products.first.name
        expect(page).to have_content products.first.display_price
      end
    end

    scenario "user clicks a related product link" do
      within("div.featuredProductsSlider") do
        click_link products.first.name
        expect(current_path).to eq potepan_product_path(products.first.id)
      end
    end
  end
end

require 'rails_helper'

RSpec.feature "Products", type: :feature do
  describe "show details of a product" do
    given(:taxon) { create(:taxon) }
    given(:product) { create(:product, taxons: [taxon]) }
    given!(:related_product) { create(:product, taxons: [taxon]) }

    background do
      visit potepan_product_path(product.id)
    end

    scenario "user visits a product details page" do
      within("div.singleProduct") do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
      within("div.productsContent") do
        expect(page).to have_link related_product.name
        expect(page).to have_link related_product.display_price
      end
    end

    scenario "user clicks a related product link" do
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    scenario "user clicks a back to index page link" do
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(product.taxons.first.id)
    end
  end
end

require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "get_related_products" do
    let!(:apple) { create(:taxon, name: "apple") }
    let!(:hardware) { create(:taxon, name: "hardware") }
    let!(:uniqlo) { create(:taxon, name: "uniqlo") }
    let!(:mac_book_pro) { create(:product, taxons: [apple, hardware]) }
    let!(:surface_pro) { create(:product, taxons: [hardware]) }
    let!(:apple_watch) { create(:product, taxons: [apple]) }
    let!(:ulrta_light_down) { create(:product, taxons: [uniqlo]) }

    context "when the product has related products" do
      it "returns related products" do
        expect(
          Spree::Product.get_related_products(mac_book_pro)
        ).to match_array [surface_pro, apple_watch]
      end
    end

    context "when the product does not have any related products" do
      it "returns empty" do
        expect(Spree::Product.get_related_products(ulrta_light_down).empty?).to be true
      end
    end
  end
end

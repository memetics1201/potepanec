require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
    let!(:unrelated_product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status 200
    end

    it "renders a correct page" do
      expect(response).to render_template :show
    end

    it "correctly assigns data to @product" do
      expect(assigns(:product)).to eq product
    end

    it "correctly assigns data to @related_products" do
      expect(assigns(:related_products)).to match_array related_products
    end

    context "when a product has more than 5 related products " do
      let!(:exceeded_related_product) { create(:product, taxons: [taxon]) }

      it "assigns data to @related_products up to 4" do
        expect(assigns(:related_products).size).to eq 4
      end
    end
  end
end

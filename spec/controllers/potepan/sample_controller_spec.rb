require 'rails_helper'

RSpec.describe Potepan::SampleController, type: :controller do
  describe "GET #index" do
    let!(:products) { create_list(:product, 8) }

    before do
      get :index
    end

    it "returns a 200 response" do
      expect(response).to have_http_status 200
    end

    it "renders a correct page" do
      expect(response).to render_template :index
    end

    it "correctly assigns data to @products" do
      expect(assigns(:products)).to match_array products
    end

    context "when products are more than 8 " do
      let!(:exceeded_product) { create(:product) }

      it "assigns data to @related_products up to 8" do
        expect(assigns(:products).size).to eq 8
      end
    end
  end
end

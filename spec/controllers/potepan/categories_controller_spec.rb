require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "get #show" do
    let(:taxon) { create(:taxon) }
    let!(:taxonomy) { create(:taxonomy) }
    let(:option_type) { create(:option_type) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status 200
    end

    it "renders a correct page" do
      expect(response).to render_template :show
    end

    it "correctly assigns data to @taxon" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "correctly assigns data to @taxonomies" do
      expect(assigns(:taxonomies)).to include taxonomy
    end

    it "correctly assigns data to @option_types" do
      expect(assigns(:option_types)).to match_array option_type
    end
  end
end

class AddClassificationsCountToTaxons < ActiveRecord::Migration[5.2]
  def self.up
    add_column :spree_taxons, :classifications_count, :integer, null: false, default: 0
  end

  def self.down
    remove_column :spree_taxons, :classifications_count
  end
end

class AddOptionValuesVariantsCountToOptionValues < ActiveRecord::Migration[5.2]
  def self.up
    add_column :spree_option_values, :option_values_variants_count, :integer, null: false, default: 0
  end

  def self.down
    remove_column :spree_option_values, :option_values_variants_count
  end
end
